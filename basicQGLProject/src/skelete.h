#ifndef SKELETE_H
#define SKELETE_H

#include <vector>
#include <queue>
#include <map>
#include <cassert>
#include <string>
#include <iostream>
#include <fstream>
#include <QString>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QDebug>
#include "point3.h"

#include <cmath>

#include <GL/glut.h>

struct Articulation {
    // membres :
    point3d p; // une position

    std::vector<unsigned int> v_indices;

    int fatherBone; // there should be only 1
    std::vector< unsigned int > childBones;

    void setFatherBone( int f ) {
        if( fatherBone >= 0 ) {
            std::cout << fatherBone << " ";
            //assert(fatherBone == f);
        }
        fatherBone = f;
    }
    bool isRoot() const {
        return fatherBone == -1;
    }

    Articulation() : fatherBone(-1) { childBones.clear(); }
    Articulation(point3d _p)
    {
        fatherBone = -1;
        childBones.clear();
        p = _p;
    }
};

struct Bone {
    // membres :
    unsigned int joints[2];

    int fatherBone; // there should be only 1
    std::vector< unsigned int > childBones;

    void setFatherBone( int f ) {
        if( fatherBone >= 0 ) {

            assert(fatherBone == f);
        }
        fatherBone = f;
    }
    bool isRoot() const {
        return fatherBone == -1;
    }

    Bone() : fatherBone(-1) { childBones.clear(); }
    Bone(int joint1, int joint2)
    {
        fatherBone = -1;
        joints[0] = joint1;
        joints[1] = joint2;
        childBones.clear();
    }
};

struct BoneTransformation{
    // membres :
    mat33d localRotation;

    mat33d world_space_rotation;
    point3d world_space_translation;

    BoneTransformation() : localRotation(mat33<float>::Identity()) ,  world_space_rotation(mat33<float>::Identity()) , world_space_translation(0,0,0) {}
};

struct SkeletonTransformation{
    // membres :
    std::vector< BoneTransformation > bone_transformations;
    std::vector< point3d > articulations_transformed_position;

    void resize( unsigned int n_bones , unsigned int n_articulations ) {
        bone_transformations.resize( n_bones );
        articulations_transformed_position.resize( n_articulations );
    }
};


class Skelete
{
public:
    Skelete();
    // membres :
    std::vector< Articulation > articulations;
    std::vector< Bone > bones;
    std::vector< unsigned int > ordered_bone_indices; // process them by order in the hierarchy

    void buildStructure() {
        ordered_bone_indices.clear();
        std::vector< unsigned int > rootBones; // why not have several

        for( unsigned int b = 0 ; b < bones.size() ; ++b ) {
            Articulation & a0 = articulations[ bones[b].joints[0] ];
            Articulation & a1 = articulations[ bones[b].joints[1] ];

            a0.childBones.push_back( b );
            a1.setFatherBone( b );
        }

        for( unsigned int aIdx = 0 ; aIdx < articulations.size() ; ++aIdx ) {
            Articulation & a = articulations[ aIdx ];
            if( a.isRoot() ) {
                for( unsigned int bIt = 0 ; bIt < a.childBones.size() ; ++bIt ) {
                    unsigned int b = a.childBones[bIt];
                    rootBones.push_back( b );
                }
            }
            else {
                unsigned int bfIdx = a.fatherBone;
                Bone & bf = bones[bfIdx];
                for( unsigned int bIt = 0 ; bIt < a.childBones.size() ; ++bIt ) {
                    unsigned int bcIdx = a.childBones[bIt];
                    Bone & bc = bones[bcIdx];
                    bc.setFatherBone( bfIdx );
                    bf.childBones.push_back( bcIdx );
                }
            }
        }

        for( unsigned int rIt = 0 ; rIt < rootBones.size() ; ++rIt ) {
            unsigned int rootboneIdx = rootBones[rIt];
            std::queue< unsigned int > bonesIndices;
            bonesIndices.push(rootboneIdx);
            while( ! bonesIndices.empty()) {
                unsigned int bIdx = bonesIndices.front();
                bonesIndices.pop();
                ordered_bone_indices.push_back(bIdx);
                Bone & b = bones[bIdx];
                for( unsigned int bIt = 0 ; bIt < b.childBones.size() ; ++bIt ) {
                    unsigned int bcIdx = b.childBones[bIt];
                    bonesIndices.push(bcIdx);
                }
            }
        }

        //assert( ordered_bone_indices.size() == bones.size() );
    }

    void load (const std::string & filename){
        std::ifstream in (filename.c_str ());
        if (!in)
            exit (EXIT_FAILURE);
        std::string tmpString;
        unsigned int sizeA;
        in >> tmpString >> sizeA;
        articulations.resize (sizeA);
        for (unsigned int i = 0; i < sizeA; i++)
            in >> articulations[i].p[0] >> articulations[i].p[1] >> articulations[i].p[2];

        unsigned int sizeB;
        in >> tmpString >> sizeB;
        bones.resize (sizeB);
        for (unsigned int i = 0; i < sizeB; i++) {
            for (unsigned int j = 0; j < 2; j++)
                in >> bones[i].joints[j];
        }
        in.close ();

        buildStructure();
    }

    void saveSkeletonToSkel(const QString& filename) const {
        QFile file(filename);
        if (file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QTextStream out(&file);

            // Écriture des articulations
            out << "ARTICULATIONS " << articulations.size() << "\n";
            for (const Articulation& articulation : articulations) {
                out << articulation.p[0] << " " << articulation.p[1] << " " << articulation.p[2] << "\n";
            }

            // Écriture des os
            out << "BONES " << bones.size() << "\n";
            for (const Bone& bone : bones) {
                out << bone.joints[0] << " " << bone.joints[1] << "\n";
            }

            file.close();
        }
        else {
            qDebug() << "Impossible d'ouvrir le fichier pour sauvegarde : " << filename;
        }
    }

    bool loadSkeletonFromSkel(const QString& filename) {
        QFile file(filename);
        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream in(&file);

            QString line;
            QStringList parts;

            // articulations
            line = in.readLine();
            parts = line.split(' ');
            if (parts.size() < 2) {
                qDebug() << "Format de fichier .skel invalide (nombre d'articulations manquant)";
                return false;
            }

            int numArticulations = parts[1].toInt();

            articulations.clear();
            for (int i = 0; i < numArticulations; ++i) {
                line = in.readLine();
                parts = line.split(' ');

                if (parts.size() < 3) {
                    qDebug() << "Format de fichier .skel invalide (données d'articulation manquantes)";
                    return false;
                }

                Articulation articulation;
                articulation.p[0] = parts[0].toFloat();
                articulation.p[1] = parts[1].toFloat();
                articulation.p[2] = parts[2].toFloat();

                articulations.push_back(articulation);
            }

            // os
            line = in.readLine();
            parts = line.split(' ');
            if (parts.size() < 2) {
                qDebug() << "Format de fichier .skel invalide (nombre d'os manquant)";
                return false;
            }

            int numBones = parts[1].toInt();

            bones.clear();
            for (int i = 0; i < numBones; ++i) {
                line = in.readLine();
                parts = line.split(' ');

                if (parts.size() < 2) {
                    qDebug() << "Format de fichier .skel invalide (données d'os manquantes)";
                    return false;
                }

                Bone bone;
                bone.joints[0] = parts[0].toUInt();
                bone.joints[1] = parts[1].toUInt();

                bones.push_back(bone);
            }

            file.close();
            return true;
        }
        else {
            qDebug() << "Impossible d'ouvrir le fichier pour chargement : " << filename;
            return false;
        }
    }

    //fonction pour creer la skelete
    void create(std::vector<point3d> _articulations)
    {
        unsigned int sizeA = _articulations.size();
        articulations.resize(sizeA);

        for(int i = 0; i < sizeA; i ++)
        {
            articulations[i].p = _articulations[i];
        }

        unsigned int sizeB = sizeA - 1;
        bones.resize(sizeB);
        for (unsigned int i = 0; i < sizeB; i++) {
            bones[i].joints[0] = i;
            bones[i].joints[1] = i + 1;
        }

        buildStructure();
    }

    void computeGlobalTransformationParameters( SkeletonTransformation & transfo ) {
        std::vector< point3d > & articulations_transformed_position = transfo.articulations_transformed_position;
        articulations_transformed_position.resize( articulations.size() );
        for( unsigned int bIt = 0 ; bIt < ordered_bone_indices.size() ; ++bIt ) {
            unsigned bIdx = ordered_bone_indices[bIt];
            Bone & b = bones[bIdx];

            if( b.isRoot() ) {
                point3d a0RestPos = articulations[ b.joints[0] ].p;
                point3d a0TargetPos = a0RestPos;
                articulations_transformed_position[ b.joints[0] ] = a0TargetPos;
                BoneTransformation & bone_transformation = transfo.bone_transformations[bIdx];
                bone_transformation.world_space_rotation = bone_transformation.localRotation;

                // set the articulation as pivot point :
                bone_transformation.world_space_translation = a0TargetPos - bone_transformation.world_space_rotation * a0RestPos;

                // update the child articulation :
                point3d a1RestPos = articulations[ b.joints[1] ].p;
                point3d a1TargetPos = bone_transformation.world_space_rotation * a1RestPos + bone_transformation.world_space_translation;
                articulations_transformed_position[ b.joints[1] ] = a1TargetPos;
            }
            else{
                point3d a0RestPos = articulations[ b.joints[0] ].p;
                point3d a0TargetPos = articulations_transformed_position[ b.joints[0] ];

                BoneTransformation & bone_transformation = transfo.bone_transformations[bIdx];
                BoneTransformation & bFatherTransfo = transfo.bone_transformations[b.fatherBone];
                bone_transformation.world_space_rotation = bFatherTransfo.world_space_rotation * bone_transformation.localRotation;

                // set the articulation as pivot point :
                bone_transformation.world_space_translation = a0TargetPos - bone_transformation.world_space_rotation * a0RestPos;

                // update the child articulation :
                point3d a1RestPos = articulations[ b.joints[1] ].p;
                point3d a1TargetPos = bone_transformation.world_space_rotation * a1RestPos + bone_transformation.world_space_translation;
                articulations_transformed_position[ b.joints[1] ] = a1TargetPos;
            }
        }
    }

    void computeGlobalIK( SkeletonTransformation & transfo ) {
        std::vector< point3d > & articulations_transformed_position = transfo.articulations_transformed_position;
        articulations_transformed_position.resize( articulations.size() );
        for( unsigned int bIt = 0 ; bIt < ordered_bone_indices.size() ; ++bIt ) {
            unsigned bIdx = ordered_bone_indices[bIt];
            Bone & b = bones[bIdx];

            if( b.isRoot() ) {
                point3d a0RestPos = articulations[ b.joints[0] ].p;
                point3d a0TargetPos = a0RestPos;
                articulations_transformed_position[ b.joints[0] ] = a0TargetPos;
                BoneTransformation & bone_transformation = transfo.bone_transformations[bIdx];
                bone_transformation.world_space_rotation = bone_transformation.localRotation;

                // set the articulation as pivot point :
                bone_transformation.world_space_translation = a0TargetPos - bone_transformation.world_space_rotation * a0RestPos;

                // update the child articulation :
                point3d a1RestPos = articulations[ b.joints[1] ].p;
                point3d a1TargetPos = bone_transformation.world_space_rotation * a1RestPos + bone_transformation.world_space_translation;
                articulations_transformed_position[ b.joints[1] ] = a1TargetPos;
            }
            else{
                point3d a0RestPos = articulations[ b.joints[0] ].p;
                point3d a0TargetPos = articulations_transformed_position[ b.joints[0] ];

                BoneTransformation & bone_transformation = transfo.bone_transformations[bIdx];
                BoneTransformation & bFatherTransfo = transfo.bone_transformations[b.fatherBone];
                mat33d world_space_rotation = bFatherTransfo.world_space_rotation * bone_transformation.localRotation;
                //bone_transformation.world_space_rotation = world_space_rotation;
                // set the articulation as pivot point :
                //bone_transformation.world_space_translation = a0TargetPos - world_space_rotation * a0RestPos;

                // update the child articulation :
                point3d a1RestPos = articulations[ b.joints[1] ].p;
                point3d a1TargetPos = world_space_rotation * a1RestPos + bone_transformation.world_space_translation;
                articulations_transformed_position[ b.joints[1] ] = a1TargetPos;
            }
        }
    }

    void computeProceduralAnim( double t , SkeletonTransformation & transfo ) {
        transfo.bone_transformations.resize( bones.size() );
        for( unsigned int bIt = 0 ; bIt < ordered_bone_indices.size() ; ++bIt ) {
            unsigned bIdx = ordered_bone_indices[bIt];
            Bone & b = bones[bIdx];
            if( b.isRoot() ) {
                BoneTransformation & bone_transformation = transfo.bone_transformations[bIdx];
                bone_transformation.localRotation = mat33d::Identity();
            }
            else{
                BoneTransformation & bone_transformation = transfo.bone_transformations[bIdx];
                point3d axis( cos( 2 * M_PI * bIt / (double)(bones.size()) )  ,  sin( 2 * M_PI * bIt / (double)(bones.size()) )  , 0.0 );
                bone_transformation.localRotation = mat33d::getRotationMatrixFromAxisAndAngle( axis , (0.25*M_PI) * cos( t ) );
            }
        }

        // update articulation positions:
        computeGlobalTransformationParameters(transfo);
    }

    void updateIKChain( SkeletonTransformation & transfoIK , unsigned int targetArticulation , point3d targetPosition , unsigned int maxIterNumber = 20 , double epsilonPrecision = 0.000001 ) {
        //---------------------------------------------------//
        //---------------------------------------------------//
        // code to change :

        // You should orient the articulation towards target position: -> find R
        // Note: you can use Mat3::getRotationMatrixAligning
        //---------------------------------------------------//
        //---------------------------------------------------//
        //---------------------------------------------------//

        for(int i = 0; i < maxIterNumber; i ++) {
            point3d dist = targetPosition - transfoIK.articulations_transformed_position[targetArticulation];

            if(dist.norm() < epsilonPrecision) {
                break;
            }

            Articulation cur_a = articulations[targetArticulation];
            int father = cur_a.fatherBone;


            while(father != -1 ) {

                Bone cur_bone = bones[father];

                if(cur_bone.isRoot())
                    break;


                int father_bone = articulations[cur_bone.joints[0]].fatherBone;

                point3d dir1 = transfoIK.articulations_transformed_position[targetArticulation] - transfoIK.articulations_transformed_position[cur_bone.joints[0]];
                point3d dir2 = targetPosition - transfoIK.articulations_transformed_position[cur_bone.joints[0]];

                mat33d rota = mat33d::getRotationMatrixMappingUnitsVec1ToVec2(dir1, dir2);

                if(!rota.isnan()) {
                    //n'a pas inverse
                    //transfoIK.bone_transformations[father].localRotation = mat33d::inverse(transfoIK.bone_transformations[father_bone].world_space_rotation) * (transfoIK.bone_transformations[father].world_space_rotation * rota);
                }

                father = articulations[cur_bone.joints[0]].fatherBone;

                computeGlobalTransformationParameters(transfoIK);

                point3d dist = targetPosition - transfoIK.articulations_transformed_position[targetArticulation];


                if(dist.norm() < epsilonPrecision) {
                    break;
                }
            }

        }

    }

    //----------------------------------------------//
    //----------------------------------------------//
    //----------------------------------------------//
    // draw functions :
    //----------------------------------------------//
    //----------------------------------------------//
    //----------------------------------------------//

    void draw( int displayedArticulation ) const {
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH);
        glDisable(GL_DEPTH_TEST);
        glLineWidth(3.0);
        glBegin (GL_LINES);
        for (unsigned int i = 0; i < bones.size (); i++) {
            glColor3f(1,0,0);
            {
                const Articulation & v = articulations[bones[i].joints[0]];
                glVertex3f (v.p[0], v.p[1], v.p[2]);
            }
            glColor3f(0.5,0,0);
            {
                const Articulation & v = articulations[bones[i].joints[1]];
                glVertex3f (v.p[0], v.p[1], v.p[2]);
            }
        }
        glEnd ();

        // we highlight the ordered bone number displayedBone
//        if( displayedBone >= 0 && displayedBone < ordered_bone_indices.size() ) {
//            displayedBone = ordered_bone_indices[displayedBone];
//            glLineWidth(8.0);
//            glBegin (GL_LINES);
//            glColor3f(1,0,0);
//            {
//                const Articulation & v = articulations[bones[displayedBone].joints[0]];
//                glVertex3f (v.p[0], v.p[1], v.p[2]);
//            }
//            glColor3f(1,0,0);
//            {
//                const Articulation & v = articulations[bones[displayedBone].joints[1]];
//                glVertex3f (v.p[0], v.p[1], v.p[2]);
//            }
//            glEnd ();
//        }

        // draw articulations:
        glEnable(GL_DEPTH_TEST);
        glPointSize(12.0);
        glBegin(GL_POINTS);
        glColor3f(0.5,0,0);
        for (unsigned int i = 0; i < articulations.size (); i++) {
            const Articulation & v = articulations[i];
            glVertex3f (v.p[0], v.p[1], v.p[2]);
        }
        glEnd();

        if( displayedArticulation >= 0 && displayedArticulation < articulations.size() ) {
            glPointSize(16.0);
            glBegin(GL_POINTS);
            glColor3f(1,0,0);
            {
                const Articulation & v = articulations[displayedArticulation];
                glVertex3f (v.p[0], v.p[1], v.p[2]);
            }
            glEnd();
        }

        glEnable(GL_DEPTH);
        glEnable(GL_DEPTH_TEST);
    }

    void drawTransformedSkeleton( int displayedBone , int displayedArticulation , SkeletonTransformation const & transfo ) const {
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH);
        glDisable(GL_DEPTH_TEST);
        glLineWidth(3.0);
        glBegin (GL_LINES);
        for (unsigned int i = 0; i < bones.size (); i++) {
            glColor3f(1,0,0);
            {
                point3d p = transfo.articulations_transformed_position[ bones[i].joints[0] ];
                glVertex3f (p[0], p[1], p[2]);
            }
            glColor3f(1,0,0);
            {
                point3d p = transfo.articulations_transformed_position[ bones[i].joints[1] ];
                glVertex3f (p[0], p[1], p[2]);
            }
        }
        glEnd ();

        // we highlight the ordered bone number displayedBone
        if( displayedBone >= 0 && displayedBone < ordered_bone_indices.size() ) {
            displayedBone = ordered_bone_indices[displayedBone];
            glLineWidth(8.0);
            glBegin (GL_LINES);
            glColor3f(1,0,0);
            {
                point3d p = transfo.articulations_transformed_position[ bones[displayedBone].joints[0] ];
                glVertex3f (p[0], p[1], p[2]);
            }
            glColor3f(1,0,0);
            {
                point3d p = transfo.articulations_transformed_position[ bones[displayedBone].joints[1] ];
                glVertex3f (p[0], p[1], p[2]);
            }
            glEnd ();
        }

        // draw articulations:
        glPointSize(12.0);
        glBegin(GL_POINTS);
        glColor3f(0.5,0,0);
        for (unsigned int i = 0; i < articulations.size (); i++) {
            point3d p = transfo.articulations_transformed_position[ i ];
            glVertex3f (p[0], p[1], p[2]);
        }
        glEnd();

        if( displayedArticulation >= 0 && displayedArticulation < articulations.size() ) {
            glPointSize(16.0);
            glBegin(GL_POINTS);
            glColor3f(1,0,0);
            {
                point3d p = transfo.articulations_transformed_position[ displayedArticulation ];
                glVertex3f (p[0], p[1], p[2]);
            }
            glEnd();
        }

        glEnable(GL_DEPTH);
        glEnable(GL_DEPTH_TEST);
    }
};

#endif // SKELETE_H
