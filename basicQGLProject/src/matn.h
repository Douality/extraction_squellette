#ifndef MATN_H
#define MATN_H


#include <fstream>
#include <vector>
#include "vecn.h"
using namespace std;

class matn
{
public:
    matn();
    matn(int rows, int columns);

    // Gets/Sets
    const double at(int i, int j) const { return m[i][j]; }
    void set(int i, int j, double value) { m[i][j] = value; }

private:
    vector< vector<double> > m;
};

vecn solveLS(matn A, vecn b);

#endif // VEC_N
