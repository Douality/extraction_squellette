#ifndef VECN_H
#define VECN_H


#include <vector>
using namespace std;

class vecn
{
public:
    vecn();
    vecn(int size);

    // Gets/Sets
    const double at(int i) const { return v[i]; }
    void set(int i, double value) { v[i] = value; }

private:
    vector<double> v;
};


#endif // VEC_N
