#ifndef MYVIEWER_H
#define MYVIEWER_H

// Mesh stuff:
#include "Mesh.h"

// Parsing:
#include "BasicIO.h"

// opengl and basic gl utilities:
#define GL_GLEXT_PROTOTYPES
#include <gl/openglincludeQtComp.h>
#include <GL/glext.h>
#include <QOpenGLFunctions_4_3_Core>
#include <QOpenGLFunctions>
#include <QGLViewer/qglviewer.h>

#include <gl/GLUtilityMethods.h>

// Qt stuff:
#include <QFormLayout>
#include <QToolBar>
#include <QColorDialog>
#include <QFileDialog>
#include <QKeyEvent>
#include <QInputDialog>
#include <QLineEdit>
#include <QLabel>


#include "qt/QSmartAction.h"


class MyViewer : public QGLViewer , public QOpenGLFunctions_4_3_Core
{
    Q_OBJECT

    Mesh mesh;
    int selected_articulation_index = -1;
    GLfloat cur_z = 1;
    bool is_adding_key_point = false;
    bool is_selecting_key_point = false;
    QString guideText = "Open a model";
    QWidget * controls;
    bool is_connecting_key_point = false;
    std::vector<Triangle> save_maillage;
    bool wireframe = false;
    bool lightning = false;

public :

    MyViewer(QGLWidget * parent = NULL) : QGLViewer(parent) , QOpenGLFunctions_4_3_Core() {

    }



    void add_actions_to_toolBar(QToolBar *toolBar)
    {
        // Specify the actions :
        DetailedAction * open_mesh = new DetailedAction( QIcon("./icons/open.png") , "Open Mesh" , "Open Mesh" , this , this , SLOT(open_mesh()) );
        DetailedAction * save_mesh = new DetailedAction( QIcon("./icons/save.png") , "Save model" , "Save model" , this , this , SLOT(save_mesh()) );
        DetailedAction * simplify_mesh = new DetailedAction( QIcon("./icons/skelete.png") , "Simplify mesh" , "Simplify mesh" , this , this , SLOT(simplify_mesh()) );
        DetailedAction * add_sleketon = new DetailedAction( QIcon("./icons/skelete.png") , "Add sleketon" , "Add sleketon" , this , this , SLOT(add_sleketon()) );
        DetailedAction * help = new DetailedAction( QIcon("./icons/help.png") , "HELP" , "HELP" , this , this , SLOT(help()) );
        DetailedAction * saveCamera = new DetailedAction( QIcon("./icons/camera.png") , "Save camera" , "Save camera" , this , this , SLOT(saveCamera()) );
        DetailedAction * openCamera = new DetailedAction( QIcon("./icons/open_camera.png") , "Open camera" , "Open camera" , this , this , SLOT(openCamera()) );
        DetailedAction * saveSnapShotPlusPlus = new DetailedAction( QIcon("./icons/save_snapshot.png") , "Save snapshot" , "Save snapshot" , this , this , SLOT(saveSnapShotPlusPlus()) );
        DetailedAction * get_skeleton = new DetailedAction( QIcon("./icons/skelete.png") , "Get sleketon" , "Get sleketon" , this , this , SLOT(get_skeleton()) );
        DetailedAction * get_skeleton2 = new DetailedAction( QIcon("./icons/skelete.png") , "Get sleketon2" , "Get sleketon2" , this , this , SLOT(get_skeleton2()) );
        DetailedAction * save = new DetailedAction( QIcon("./icons/save_snapshot.png") , "Save skeleton" , "Save skeleton" , this , this , SLOT(save_skeleton()) );
        DetailedAction * load = new DetailedAction( QIcon("./icons/open.png") , "load skeleton" , "load skeleton" , this , this , SLOT(load_squeleton()) );
        DetailedAction * associate = new DetailedAction( QIcon("./icons/help.png") , "associate skeleton" , "associate skeleton" , this , this , SLOT(associate_squeleton()) );

        // Add them :

        toolBar->addAction( open_mesh );
        toolBar->addAction( save_mesh );
        toolBar->addAction( simplify_mesh );
        toolBar->addAction( add_sleketon );
        toolBar->addAction( help );
        toolBar->addAction( saveCamera );
        toolBar->addAction( openCamera );
        toolBar->addAction( saveSnapShotPlusPlus );
        toolBar->addAction( get_skeleton );
        toolBar->addAction( get_skeleton2 );
        toolBar->addAction( save );
        toolBar->addAction( load );
        //toolBar->addAction( associate );
        //

    }


    void draw() {
        if(this->lightning){
            glEnable(GL_DEPTH);
            glEnable(GL_DEPTH_TEST);
            glEnable(GL_LIGHTING);
        }
        else{
            glDisable(GL_DEPTH);
            glDisable(GL_DEPTH_TEST);
            glDisable(GL_LIGHTING);
        }

        if (this->wireframe) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }

        //original mesh
        //glColor3f(0.5, 0.5, 0.8);
        //glColor3f(0.0, 0.0, 1.0);
        glColor3f(0.5, 0.7, 1.0);

        glBegin(GL_TRIANGLES);
        for( unsigned int t = 0 ; t < mesh.triangles.size() ; ++t ) {
            point3d const & p0 = mesh.vertices[ mesh.triangles[t][0] ].p;
            point3d const & p1 = mesh.vertices[ mesh.triangles[t][1] ].p;
            point3d const & p2 = mesh.vertices[ mesh.triangles[t][2] ].p;
            point3d const & n = point3d::cross( p1-p0 , p2-p0 ).direction();
            glNormal3f(n[0],n[1],n[2]);
            glVertex3f(p0[0],p0[1],p0[2]);
            glVertex3f(p1[0],p1[1],p1[2]);
            glVertex3f(p2[0],p2[1],p2[2]);
        }
        glEnd();

        //simplied mesh
        glDisable(GL_DEPTH);
        glDisable(GL_DEPTH_TEST);

        //glColor3f(0.5, 0.5, 0.6);
        //glColor3f(0.0, 1.0, 0.0);
        //glColor3f(0.2, 0.8, 0.2);
        glColor3f(0.5, 0.0, 1.0);

        glBegin(GL_TRIANGLES);
        for( unsigned int t = 0 ; t < mesh.simplified_triangles.size() ; ++t ) {
            point3d const & p0 = mesh.simplified_vertices[ mesh.simplified_triangles[t][0] ].p;
            point3d const & p1 = mesh.simplified_vertices[ mesh.simplified_triangles[t][1] ].p;
            point3d const & p2 = mesh.simplified_vertices[ mesh.simplified_triangles[t][2] ].p;
            point3d const & n = point3d::cross( p1-p0 , p2-p0 ).direction();
            glNormal3f(n[0],n[1],n[2]);
            glVertex3f(p0[0],p0[1],p0[2]);
            glVertex3f(p1[0],p1[1],p1[2]);
            glVertex3f(p2[0],p2[1],p2[2]);
        }
        glEnd();

        if(this->wireframe){
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }


        glEnable(GL_DEPTH_TEST);
        //draw skelete
        mesh.draw_skelete(selected_articulation_index);

        //
        glColor3f(0.2,0.2,0.8);
        drawText(20, 30, guideText);

        if(this->wireframe){
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        //drawEdges();
    }

    void drawEdges() {
        glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH);
        glDisable(GL_DEPTH_TEST);
        glLineWidth(2.0);
        glBegin(GL_LINES);

        for(Edge currentEdge : mesh.edges){

            point3d const & p1 = currentEdge.v1.p;
            point3d const & p2 = currentEdge.v2.p;

            glColor3f(1,0,0);
            glVertex3f(p1[0], p1[1], p1[2]);

            glColor3f(1,1,1);
            glVertex3f(p2[0], p2[1], p2[2]);
        }

        glEnd();
    }

    void pickBackgroundColor() {
        QColor _bc = QColorDialog::getColor( this->backgroundColor(), this);
        if( _bc.isValid() ) {
            this->setBackgroundColor( _bc );
            this->update();
        }
    }

    void adjustCamera( point3d const & bb , point3d const & BB ) {
        point3d const & center = ( bb + BB )/2.f;
        setSceneCenter( qglviewer::Vec( center[0] , center[1] , center[2] ) );
        setSceneRadius( 1.5f * ( BB - bb ).norm() );
        showEntireScene();
    }


    void init() {
        makeCurrent();
        initializeOpenGLFunctions();

        setMouseTracking(true);// Needed for MouseGrabber.

        setBackgroundColor(QColor(255,255,255));

        // Lights:
        GLTools::initLights();
        GLTools::setSunsetLight();
        GLTools::setDefaultMaterial();

        //
        glShadeModel(GL_SMOOTH);
        glFrontFace(GL_CCW); // CCW ou CW

        glEnable(GL_DEPTH);
        glEnable(GL_DEPTH_TEST);
        glDepthFunc(GL_LEQUAL);

        glEnable(GL_CLIP_PLANE0);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glEnable(GL_COLOR_MATERIAL);

        //
        setSceneCenter( qglviewer::Vec( 0 , 0 , 0 ) );
        setSceneRadius( 10.f );
        showEntireScene();
    }

    QString helpString() const {
        QString text("<h2>Our cool project</h2>");
        text += "<p>";
        text += "This is a research application, it can explode.";
        text += "<h3>Participants</h3>";
        text += "<ul>";
        text += "<li>Blanchard D, NGUYEN T.H</li>";
        text += "</ul>";
        text += "<h3>Basics</h3>";
        text += "<p>";
        text += "<ul>";
        text += "<li>H   :   make this help appear</li>";
        text += "<li>Ctrl + mouse right button double click   :   choose background color</li>";
        text += "<li>Ctrl + T   :   change window title</li>";
        text += "<li>M   :   toogle supperpositon of original mesh /li>";
        text += "<li>L   :   toogle ligthning /li>";
        text += "<li>W   :   toogle Wireframe</li>";
        text += "<li>A   :   if contraction add point</li>";
        text += "<li>E   :   if contraction comgine point<</li>";
        text += "<li>SUPPR   :   if contraction delete point<</li>";
        text += "</ul>";
        return text;
    }

    void updateTitle( QString text ) {
        this->setWindowTitle( text );
        emit windowTitleUpdated(text);
    }

    void keyPressEvent( QKeyEvent * event ) {
            if( event->key() == Qt::Key_H ) {
                help();
            }
            else if( event->key() == Qt::Key_T ) {
                if( event->modifiers() & Qt::CTRL )
                {
                    bool ok;
                    QString text = QInputDialog::getText(this, tr(""), tr("title:"), QLineEdit::Normal,this->windowTitle(), &ok);
                    if (ok && !text.isEmpty())
                    {
                        updateTitle(text);
                    }
                }
            }
            else if(event->key() == Qt::Key_Delete) {
                if(selected_articulation_index != -1)
                {
                    mesh.delete_articulation(selected_articulation_index);
                    selected_articulation_index = -1;
                    update();
                }
            }
            else if(event->key() == Qt::Key_A) {
                if(selected_articulation_index != -1)
                {
                    is_adding_key_point = true;
                    guideText = "Click to add keypoint";
                    update();
                }
            }
            else if(event->key() == Qt::Key_E) {
                if(selected_articulation_index != -1)
                {
                    is_connecting_key_point = true;
                    guideText = "Choose another keypoint";
                    update();
                }
            }
            else if(event->key() == Qt::Key_M) {
                if(save_maillage.size() > 0){
                    mesh.triangles = save_maillage;
                    save_maillage.clear();
                    update();
                    draw();
                }
                else{
                    save_maillage = std::vector<Triangle>(mesh.triangles);
                    mesh.triangles.clear();
                    update();
                    draw();
                }
            }
            else if(event->key() == Qt::Key_W) {
                wireframe = !wireframe;
                update();
                draw();
            }
            else if(event->key() == Qt::Key_D) {
                lightning = !lightning;
                update();
                draw();
            }
        }

    void mouseDoubleClickEvent( QMouseEvent * e )
    {
        if( (e->modifiers() & Qt::ControlModifier)  &&  (e->button() == Qt::RightButton) )
        {
            pickBackgroundColor();
            return;
        }

        if( (e->modifiers() & Qt::ControlModifier)  &&  (e->button() == Qt::LeftButton) )
        {
            showControls();
            return;
        }

        QGLViewer::mouseDoubleClickEvent( e );
    }

    point3d get_world_coordinate(QMouseEvent* e)
    {
        GLint viewport[4];
        GLdouble modelview[16];
        GLdouble projection[16];
        GLfloat winX, winY, winZ;
        GLdouble posX, posY, posZ;

        glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
        glGetDoublev( GL_PROJECTION_MATRIX, projection );
        glGetIntegerv( GL_VIEWPORT, viewport );

        winX = e->x();
        winY = height() - e->y();
        glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);

        gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
        //std::cout << posX << " " << posY << " " << posZ << std::endl;

        cur_z = winZ;
        return point3d(posX, posY, posZ);
    }

    point3d get_world_coordinate_from_fix_z(QMouseEvent* e, GLfloat winZ)
    {

        GLint viewport[4];
        GLdouble modelview[16];
        GLdouble projection[16];
        GLfloat winX, winY;
        GLdouble posX, posY, posZ;

        glGetDoublev( GL_MODELVIEW_MATRIX, modelview );
        glGetDoublev( GL_PROJECTION_MATRIX, projection );
        glGetIntegerv( GL_VIEWPORT, viewport );

        winX = e->x();
        winY = height() - e->y();

        gluUnProject( winX, winY, winZ, modelview, projection, viewport, &posX, &posY, &posZ);
        //std::cout << posX << " " << posY << " " << posZ << " " << winZ << std::endl;

        return point3d(posX, posY, posZ);
    }

    void mousePressEvent(QMouseEvent* e ) {

            if(is_adding_key_point == false)
            {
                point3d p = get_world_coordinate(e);
                int a_index = mesh.pick_articulation(p);

                if(is_connecting_key_point == true) {
                    if(a_index != -1){
                        is_connecting_key_point = false;
                        mesh.connect_keypoints(selected_articulation_index, a_index);
                        selected_articulation_index = a_index;
                        guideText = "a: add a new keypoint \n, "
                                    "e: connect keypoints \n, "
                                    "delete: delete the selected keypoint";
                        update();
                    }
                    return;
                }

                if(a_index != -1)
                {
                    std::cout << a_index << std::endl;
                    is_selecting_key_point = true;
                    selected_articulation_index = a_index;
                    //hightlight
                    guideText = "a: add a new keypoint \n, "
                                "e: connect keypoints \n, "
                                "delete: delete selected keypoint";
                    update();
                }
                else
                {
                    selected_articulation_index = -1;
                    guideText = "Select a keypoint";
                    QGLViewer::mousePressEvent(e);
                }
            }
            else
            {
                is_adding_key_point = false;
                point3d p = get_world_coordinate_from_fix_z(e, cur_z);
                int n_index = mesh.add_articulation(p, selected_articulation_index);
                selected_articulation_index = n_index;
                guideText = "a: add a new keypoint, \n"
                            "e: connect keypoints, \n"
                            "delete: delete the selected keypoint, \n"
                            "M toogle supperposition, \n"
                            "D toogle Light, \n"
                            "W toogle wireframe";
                update();

            }

        }

    void mouseMoveEvent(QMouseEvent* e  ){


        if(is_selecting_key_point)
        {
            point3d p = get_world_coordinate_from_fix_z(e, cur_z);
            mesh.update_articulation(selected_articulation_index, p);
            update();
        }
        QGLViewer::mouseMoveEvent(e);
    }

    void mouseReleaseEvent(QMouseEvent* e  ) {
        is_selecting_key_point = false;
        QGLViewer::mouseReleaseEvent(e);
    }

signals:
    void windowTitleUpdated( const QString & );

public slots:
    void open_mesh() {
        mesh.triangles.clear();
        mesh.simplified_triangles.clear();
        mesh.vertices.clear();
        mesh.simplified_vertices.clear();
        mesh.edges.clear();
        mesh.skeleton.articulations.clear();
        mesh.skeleton.bones.clear();
        bool success = false;
        QString fileName = QFileDialog::getOpenFileName(NULL,"","");
        if ( !fileName.isNull() ) { // got a file name
            if(fileName.endsWith(QString(".off")))
                success = OFFIO::openTriMesh(fileName.toStdString() , mesh.vertices , mesh.triangles );
            else if(fileName.endsWith(QString(".obj")))
                success = OBJIO::openTriMesh(fileName.toStdString() , mesh.vertices , mesh.triangles );
            if(success) {
                std::cout << fileName.toStdString() << " was opened successfully" << std::endl;
                point3d bb(FLT_MAX,FLT_MAX,FLT_MAX) , BB(-FLT_MAX,-FLT_MAX,-FLT_MAX);
                for( unsigned int v = 0 ; v < mesh.vertices.size() ; ++v ) {
                    bb = point3d::min(bb , mesh.vertices[v]);
                    BB = point3d::max(BB , mesh.vertices[v]);
                }
                adjustCamera(bb,BB);
                guideText = "Open model successfully \n,"
                            "M supperposition, \n"
                            "D Light, \n"
                            "W wireframe";
                update();
            }
            else
                std::cout << fileName.toStdString() << " could not be opened" << std::endl;
        }
    }

    void save_mesh() {
        bool success = false;
        QString fileName = QFileDialog::getOpenFileName(NULL,"","");
        if ( !fileName.isNull() ) { // got a file name
            if(fileName.endsWith(QString(".off")))
                success = OFFIO::save(fileName.toStdString() , mesh.vertices , mesh.triangles );
            else if(fileName.endsWith(QString(".obj")))
                success = OBJIO::save(fileName.toStdString() , mesh.vertices , mesh.triangles );
            if(success)
                std::cout << fileName.toStdString() << " was saved" << std::endl;
            else
                std::cout << fileName.toStdString() << " could not be saved" << std::endl;
        }
    }

    void save_skeleton(){
        QString filename = QFileDialog::getSaveFileName(nullptr, "Enregistrer le squelette", "", "Fichier SKEL (*.skel)");
        mesh.skeleton.saveSkeletonToSkel(filename);
    }

    void load_squeleton(){
        QString filename = QFileDialog::getOpenFileName(nullptr, "Enregistrer le squelette", "", "Fichier SKEL (*.skel)");
        mesh.skeleton.loadSkeletonFromSkel(filename);
    }

    void simplify_mesh() {
        mesh.simplify(15);
        for(const Vertex& v : mesh.vertices){
            if(std::isnan(v[0]) || std::isnan(v[1]) || std::isnan(v[2])) std::cout << "Nan : " << v << std::endl;
            if(std::isinf(v[0]) || std::isinf(v[1]) || std::isinf(v[2])) std::cout << "Infinity : " << v << std::endl;
        }
        std::cout << "End simplify" << std::endl;
    }

    void add_sleketon(){
        bool ok;
        int n = QInputDialog::getInt(
                    nullptr,
                    "Choisir nombre d'itérations", "Entrez la valeur de n :",
                    20, 1, 100, 1, &ok);
        if (ok) {
            mesh.add_sleketon(n);
        }
        else {
            mesh.add_sleketon();
        }
        update();
        for(const Vertex& v : mesh.vertices){
            if(std::isnan(v[0]) || std::isnan(v[1]) || std::isnan(v[2])) std::cout << "Nan : " << v << std::endl;
            if(std::isinf(v[0]) || std::isinf(v[1]) || std::isinf(v[2])) std::cout << "Infinity : " << v << std::endl;
        }
        std::cout << "End Contraction" << std::endl;
    }

    void get_skeleton(){
        QStringList items;
        items << "false" << "true";

        bool ok;
        QString item = QInputDialog::getItem(nullptr,
                                             "Essayer auto-reconstruction (Min-dist)",
                                             "Oui ou Non ? :",
                                             items, 0, false, &ok);
        if (ok && !item.isEmpty()) {
            bool b = (item.toLower() == "true");

            this->find_skeleton(false, b);
        }

    }

    void get_skeleton2(){
        QStringList items;
        items << "false" << "true";

        bool ok;
        QString item = QInputDialog::getItem(nullptr,
                                             "Essayer auto-reconnection (Min-dist)",
                                             "Oui ou Non ? :",
                                             items, 0, false, &ok);

        bool ok2;
        QString item2 = QInputDialog::getItem(nullptr,
                                             "Essayer Collapse",
                                             "Oui ou Non ? :",
                                             items, 0, false, &ok2);
        if (ok && ok2 && !item.isEmpty() && !item2.isEmpty()) {
            bool b = (item.toLower() == "true");
            bool b2 = (item2.toLower() == "true");

            this->find_skeleton(true, b, b2);
        }
    }

    void find_skeleton(bool version=false, bool auto_completion=false, bool collapse2 = false) {
        unsigned int v = mesh.vertices.size();
        unsigned int t = mesh.triangles.size();
        unsigned int e = mesh.edges.size();
        mesh.half_edge_collapse(version, auto_completion, collapse2);
        mesh.load_skelete();
        guideText = "Select a keypoint";
        update();
        std::cout << "Number of vertices : " << v << " --> " << mesh.vertices.size() << std::endl;
        std::cout << "Number of triangles : " << t << " --> " << mesh.triangles.size() << std::endl;
        std::cout << "Number of edges : " << e << " --> " << mesh.edges.size() << std::endl;
    }

    void associate_squeleton(){
        Utilities().associateVerticesToArticulations(mesh.skeleton, mesh.vertices);
        std::cout << "Associated" << std::endl;
    }

    void showControls()
    {
        // Show controls :
        controls->close();
        controls->show();
    }

    void saveCameraInFile(const QString &filename){
        std::ofstream out (filename.toUtf8());
        if (!out)
            exit (EXIT_FAILURE);
        // << operator for point3 causes linking problem on windows
        out << camera()->position()[0] << " \t" << camera()->position()[1] << " \t" << camera()->position()[2] << " \t" " " <<
                                          camera()->viewDirection()[0] << " \t" << camera()->viewDirection()[1] << " \t" << camera()->viewDirection()[2] << " \t" << " " <<
                                          camera()->upVector()[0] << " \t" << camera()->upVector()[1] << " \t" <<camera()->upVector()[2] << " \t" <<" " <<
                                          camera()->fieldOfView();
        out << std::endl;

        out.close ();
    }

    void openCameraFromFile(const QString &filename){

        std::ifstream file;
        file.open(filename.toStdString().c_str());

        qglviewer::Vec pos;
        qglviewer::Vec view;
        qglviewer::Vec up;
        float fov;

        file >> (pos[0]) >> (pos[1]) >> (pos[2]) >>
                                                    (view[0]) >> (view[1]) >> (view[2]) >>
                                                                                           (up[0]) >> (up[1]) >> (up[2]) >>
                                                                                                                            fov;

        camera()->setPosition(pos);
        camera()->setViewDirection(view);
        camera()->setUpVector(up);
        camera()->setFieldOfView(fov);

        camera()->computeModelViewMatrix();
        camera()->computeProjectionMatrix();

        update();
    }


    void openCamera(){
        QString fileName = QFileDialog::getOpenFileName(NULL,"","*.cam");
        if ( !fileName.isNull() ) {                 // got a file name
            openCameraFromFile(fileName);
        }
    }
    void saveCamera(){
        QString fileName = QFileDialog::getSaveFileName(NULL,"","*.cam");
        if ( !fileName.isNull() ) {                 // got a file name
            saveCameraInFile(fileName);
        }
    }

    void saveSnapShotPlusPlus(){
        QString fileName = QFileDialog::getSaveFileName(NULL,"*.png","");
        if ( !fileName.isNull() ) {                 // got a file name
            setSnapshotFormat("PNG");
            setSnapshotQuality(100);
            saveSnapshot( fileName );
            saveCameraInFile( fileName+QString(".cam") );
        }
    }
};




#endif // MYVIEWER_H
