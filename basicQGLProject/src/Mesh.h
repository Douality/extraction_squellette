#ifndef PROJECTMESH_H
#define PROJECTMESH_H

#include <iostream>
#include <vector>
#include <queue>
#include <set>
#include "point3.h"
#include "skelete.h"
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseLU>

struct Voxel {
    point3d vertex = point3d(0,0,0);
    int vertex_count = 0;
};


struct Vertex{
    point3d p;
    Vertex() {}
    Vertex(double x , double y , double z) : p(x,y,z) {}
    double & operator [] (unsigned int c) { return p[c]; }
    double operator [] (unsigned int c) const { return p[c]; }
    bool operator==(const Vertex &other) const {
        double epsilon = 5.0;
        return std::abs(p[0] - other.p[0]) < epsilon &&
               std::abs(p[1] - other.p[1]) < epsilon &&
               std::abs(p[2] - other.p[2]) < epsilon;
    }

    Vertex& operator+=(const Vertex& other) {
        p[0] += other.p[0];
        p[1] += other.p[1];
        p[2] += other.p[2];
        return *this;
    }

    Vertex& operator-=(const Vertex& other) {
        p[0] += ((-1)*other.p[0]);
        p[1] += ((-1)*other.p[1]);
        p[2] += ((-1)*other.p[2]);
        return *this;
    }

    Vertex& operator/=(double d) {
        p[0] /= d;
        p[1] /= d;
        p[2] /= d;
        return *this;
    }

    Vertex& operator*=(double d) {
        p[0] /= (1/d);
        p[1] /= (1/d);
        p[2] /= (1/d);
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os, const Vertex& vertex) {
        os << "(" << vertex.p[0] << ", "
           << vertex.p[1] << ", "
           << vertex.p[2] << ")";
        return os;
    }
};

struct Triangle{
    unsigned int corners[3];
    unsigned int & operator [] (unsigned int c) { return corners[c]; }
    unsigned int operator [] (unsigned int c) const { return corners[c]; }
    unsigned int size() const { return 3 ; }
    Triangle() {}
    Triangle(int v1, int v2, int v3) {
        corners[0] = v1;
        corners[1] = v2;
        corners[2] = v3;
    }

    int has_edge(unsigned int v1,unsigned int v2) {

        if((corners[0] == v1 && corners[1] == v2) || (corners[0] == v2 && corners[1] == v1)) {
            return corners[2];
        }

        if((corners[0] == v1 && corners[2] == v2) || (corners[0] == v2 && corners[2] == v1)) {
            return corners[1];
        }

        if((corners[1] == v1 && corners[2] == v2) || (corners[1] == v2 && corners[2] == v1)) {
            return corners[0];
        }

        return -1;
    }

    friend std::ostream& operator<<(std::ostream& os, const Triangle& triangle){
        os << "(" << triangle.corners[0] << ", "
           << triangle.corners[1] << ", "
           << triangle.corners[2] << ")";
        return os;
    }
};

struct Edge{
    Vertex v1, v2;
    unsigned int i1, i2;
    double taille;

    Edge() {};
    ~Edge() {};
    Edge(const Edge& other) :
        v1(other.v1), v2(other.v2), i1(other.i1), i2(other.i2),
        taille(other.taille)
        {};

    Edge(Vertex a, Vertex b) :
        v1(a) , v2(b) {
        taille = (double)(abs(sqrt(pow(v2[0] - v1[0], 2)
                                    + pow(v2[1] - v1[1], 2)
                                    + pow(v2[2] - v1[2], 2))));}

    Edge(unsigned int a, unsigned int b) : i1(a) , i2(b) {}

    Edge(unsigned int a, unsigned int b, double l) : i1(a) , i2(b) , taille(l)
    {}

    Edge(Vertex a, Vertex b, unsigned int a1, unsigned int b2) :
        v1(a) , v2(b), i1(a1) , i2(b2){
        taille = (double)(abs(sqrt(pow(v2[0] - v1[0], 2)
                                    + pow(v2[1] - v1[1], 2)
                                    + pow(v2[2] - v1[2], 2)))); }

    unsigned int operator [] (unsigned int c) const {
        if(c == 1) return i1;
        else return i2;
    }

    bool operator<(const Edge& other) const {
        return taille > other.taille;
    }

    bool operator==(const Edge& other) const{
        return (this->i1 == other.i1) && (this->i2 == other.i2);
    }

    bool sym_equal(const Edge& other) const{
        return ((this->i1 == other.i1) && (this->i2 == other.i2))
                ||
                ((this->i1 == other.i2) && (this->i2 == other.i1));
    }

    friend std::ostream& operator<<(std::ostream& os, const Edge& edge) {
        os << "(" << edge.i1 << ", " << edge.i2 << ", " << edge.taille << ")";
        return os;
    }
};

class Utilities{
public:

    double edge_length(Vertex v1 , Vertex v2){
        return (double) (sqrt(pow(v2[0] - v1[0], 2)
                              + pow(v2[1] - v1[1], 2)
                              + pow(v2[2] - v1[2], 2)));
    }

    bool contain(std::vector<unsigned int> const& i_vector, unsigned int element){
        for (unsigned int i = 0; i < i_vector.size(); i++) {
            if (i_vector[i] == element) return true;
        }
        return false;
    }

    std::vector<std::vector<unsigned int> > collect_one_ring (
        std::vector<Vertex> const & i_vertices,
        std::vector< Triangle > const & i_triangles){

        std::vector<std::vector<unsigned int> > o_one_ring;
        o_one_ring.clear();
        o_one_ring.resize(i_vertices.size());
        for (unsigned int i = 0; i < i_triangles.size(); i++) {
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    if (j != k) {
                        if (!contain(o_one_ring[i_triangles[i][j]],
                                     i_triangles[i][k])) {
                            o_one_ring[i_triangles[i][j]].push_back(
                                i_triangles[i][k]);
                        }
                    }
                }
            }
        }
        return o_one_ring;
    }

    Eigen::MatrixXd get_k(const unsigned int i,
                          std::vector<Vertex> const & vertices,
                          std::vector<Triangle> const & triangles){

        Eigen::Vector3d pointI(vertices[i].p[0],
                               vertices[i].p[1],
                               vertices[i].p[2]);

        std::vector<unsigned int> voisins_i =
            Utilities().collect_one_ring(vertices, triangles)[i];

        Eigen::MatrixXd K(4,4);
        K.setZero();

        Eigen::Vector3d a, b, pointJ;
        Eigen::MatrixXd Kij_t, Kp;

        for(unsigned int _i : voisins_i){

            pointJ = Eigen::Vector3d(vertices[_i].p[0],
                                     vertices[_i].p[1],
                                     vertices[_i].p[2]);

            a = pointJ - pointI;
            a.normalize();

            b = a.cross(pointI);

            Eigen::MatrixXd Kij(3, 4);
            Kij << 0, -a[2], a[1], -b[0],
                a[2], 0, -a[0], -b[1],
                -a[1], a[0], 0, -b[2];

            Kij_t = Kij.transpose();

            Kp = (Kij_t * Kij);

            K += Kp;
        }
        return K;
    }

    double Euclidien(const point3d& p1, const point3d& p2) {
        double dx = p1[0] - p2[0];
        double dy = p1[1] - p2[1];
        double dz = p1[2] - p2[2];

        return std::sqrt(dx * dx + dy * dy + dz * dz);
    }

    unsigned int findClosestArticulation(Skelete& skelete, const point3d& vertex, double seuil = 0.1f) const {
        double min_distance = std::numeric_limits<double>::max();
        unsigned int closest_articulation = 0;

        for (unsigned int i = 0; i < skelete.articulations.size(); ++i) {
            double distance = Utilities().Euclidien(vertex, skelete.articulations[i].p);

            if (distance < min_distance && distance < seuil) {
                min_distance = distance;
                closest_articulation = i;
            }
        }

        return closest_articulation;
    }

    void associateVerticesToArticulations(Skelete& skelete, std::vector<Vertex> _v) {
        for (Articulation& articulation : skelete.articulations) {
            articulation.v_indices.clear();
        }

        for (unsigned int v_index = 0; v_index < _v.size(); ++v_index) {
            unsigned int closest_articulation = Utilities().findClosestArticulation(skelete, _v[v_index]);
            skelete.articulations[closest_articulation].v_indices.push_back(v_index);
        }
    }

    void translateArticulation(Articulation& articulation, const point3d& translation, std::vector<Vertex>& _v) {
        articulation.p = articulation.p + translation;

        for (unsigned int j = 0; j < articulation.v_indices.size(); ++j) {
            unsigned int vertexIndex = articulation.v_indices[j];

            if (vertexIndex < _v.size()) {
                _v[vertexIndex].p = _v[vertexIndex].p + translation;
            }
        }
    }
};

struct cost_edge{

    Edge edge;
    Eigen::MatrixXd Ki;
    Eigen::MatrixXd Kj;
    double shampling;
    double shape;

    ~cost_edge(){};
    cost_edge(){
        this->shampling = 0.0;
        this->shape = 0.0;
        this->Ki = Eigen::MatrixXd(4, 4);
        this->Ki.setZero();
        this->Kj = Eigen::MatrixXd(4, 4);
        this->Kj.setZero();
    };

    cost_edge(Edge ed) : edge(ed) {
        this->Ki = Eigen::MatrixXd(4, 4);
        this->Ki.setZero();
        this->Kj = Eigen::MatrixXd(4, 4);
        this->Kj.setZero();
        this->shape = FLT_MAX;
        this->shampling = FLT_MAX;
    }

    double shampling_cost(std::vector<Vertex> const & vertices,
                          std::vector<Triangle> const & triangles){
        std::vector<unsigned int> voisins =
            Utilities().collect_one_ring(vertices, triangles)[edge.i2];

        Vertex v1 = vertices[this->edge.i2];
        Vertex v2 = vertices[this->edge.i1];
        double cost = (sqrt(pow(v1[0] - v2[0], 2)
                            + pow(v1[1] - v2[1], 2)
                            + pow(v1[2] - v2[2], 2)));
        double somme = 0.0;

        for(unsigned int _i : voisins){
            v2 = vertices[_i];
            somme += sqrt(pow(v1[0] - v2[0], 2)
                          + pow(v1[1] - v2[1], 2)
                          + pow(v1[2] - v2[2], 2)
                          );
        }

        return (cost*somme);
    }

    double shape_cost(std::vector<Vertex> const & vertices){
        double cost = 0.;

        Eigen::Vector3d pointJ(vertices[edge.i2].p[0],
                               vertices[edge.i2].p[1],
                               vertices[edge.i2].p[2]);

        Eigen::Vector4d pj;
        pj << pointJ, 1.0;

        double result_i = pj.transpose() * this->Ki * pj;
        double result_j = pj.transpose() * this->Kj * pj;

        cost = result_i + result_j;

        return cost;
    }

    double total_cost() const{
        return 1.0 * shape + 0.1 * shampling;
    }

    bool operator<(const cost_edge& other) const{
        return total_cost() < other.total_cost();
    }

    friend std::ostream& operator<<(std::ostream& os, const cost_edge& edge) {
        os << "(" << edge.edge.i1 << ", "
           << edge.edge.i2 << ", ["
           << edge.total_cost() << " = <"
           << edge.shampling << ","
           << edge.shape <<"> ])";
        return os;
    }
};


struct Coorelation{
    std::map<unsigned int, bool> corr;

    void clear(){
        for(auto it = corr.begin(); it != corr.end(); ++it){
            corr[it->first] = false;
        }
    }

    bool isElementPresent(unsigned int key) {
        return corr.find(key) != corr.end();
    }

    void add(unsigned int key, bool value) {
        corr[key] = value;
    }

    void update(unsigned int key, bool value) {
        if (isElementPresent(key)) {
            corr[key] = value;
        }
        else {
            add(key, value);
        }
    }

    bool getValue(unsigned int key) {
        if (isElementPresent(key)) {
            return corr.at(key);
        }
        else{
            return false;
        }
    }

    bool get(unsigned int c) {
        return corr[c];
    }

    bool isOk(cost_edge e){
        return (corr[e.edge.i2] == true);
    }
};

struct EdgeQueue {
    std::priority_queue<cost_edge> edgePriorityQueue;
    Coorelation corr;
    double seuil;

    ~EdgeQueue(){};
    EdgeQueue() : seuil(FLT_MAX) {}
    EdgeQueue(double d) : seuil(d) {}

    void clear(){
        this->edgePriorityQueue = std::priority_queue<cost_edge>();
        this->corr.clear();
    }

    void clean(){
        this->edgePriorityQueue = std::priority_queue<cost_edge>();
    }

    bool isOk(cost_edge e){
        return corr.isOk(e);
    }

    void disable(unsigned int i){
        corr.corr[i] = false;
    }

    void deleteEdge(cost_edge e) {
        std::priority_queue<cost_edge> tempQueue;

        while (!edgePriorityQueue.empty()) {
            cost_edge currentEdge = edgePriorityQueue.top();
            edgePriorityQueue.pop();
            if ((currentEdge.edge.i1 == e.edge.i1 &&
                 currentEdge.edge.i2 == e.edge.i2)
                ||
                (currentEdge.edge.i1 == e.edge.i2 &&
                 currentEdge.edge.i2 == e.edge.i1)) {
                ;
            }
            else{
                tempQueue.push(currentEdge);
            }
        }

        edgePriorityQueue = tempQueue;
    }

    void deleteEdge(unsigned int source) {
        std::priority_queue<cost_edge> tempQueue;

        while (!edgePriorityQueue.empty()) {
            cost_edge currentEdge = edgePriorityQueue.top();
            edgePriorityQueue.pop();
            if (currentEdge.edge.i1 == source ||
                currentEdge.edge.i2 == source ) {
                ;
            }
            else{
                tempQueue.push(currentEdge);
            }
        }

        edgePriorityQueue = tempQueue;
    }

    cost_edge pop() {
        if (!edgePriorityQueue.empty()) {
            cost_edge highestPriorityEdge = edgePriorityQueue.top();
            edgePriorityQueue.pop();
            return highestPriorityEdge;
        }
        else {
            return cost_edge();
        }
    }

    void push(cost_edge ed){
        this->edgePriorityQueue.push(ed);
        this->corr.add(ed.edge.i1, true);
        this->corr.add(ed.edge.i1, true);
    }

    bool isEmpty(){
        return this->edgePriorityQueue.size() <= 0;
    }

    bool isFalse(){
        for(auto it = corr.corr.begin() ; it != corr.corr.end(); ++it){
            if(it->second == true) return true;
        }
        return false;
    }
};

struct Mesh{
    std::vector< Vertex > vertices;
    std::vector< Triangle > triangles;
    std::vector< Vertex > simplified_vertices;
    std::vector< Triangle > simplified_triangles;
    std::vector< Edge > edges;
    Skelete skeleton;

    void addEdge(std::vector< Edge > edges , const Edge& newEdge) {
        if (std::find(edges.begin(), edges.end(),
                      newEdge) == edges.end() &&
            std::find(edges.begin(), edges.end(),
                      Edge(newEdge.i2, newEdge.i1)) == edges.end()) {

            edges.push_back(newEdge);
        }
    }

    void set_edgequeue(EdgeQueue& edgequeue,
                       std::vector<Vertex> const & i_vertices,
                       std::vector<Triangle> const & i_triangles,
                       std::map<unsigned int, Eigen::MatrixXd>& pr){

        for(unsigned int i = 0 ; i < i_vertices.size(); ++i){
            std::vector<unsigned int> voisins =
                Utilities().collect_one_ring(i_vertices, i_triangles)[i];

            for(const unsigned int& _i : voisins){
                cost_edge ce = cost_edge();
                ce.edge.i1 = i;
                ce.edge.i2 = _i;
                ce.edge.v1 = i_vertices[i];
                ce.edge.v2 = i_vertices[_i];
                ce.Ki = pr[i];
                ce.Kj = pr[_i];
                ce.shape = ce.shape_cost(i_vertices);
                ce.shampling = ce.shampling_cost(i_vertices, i_triangles);
                edgequeue.push(ce);
            }
        }
    }

    void half_edge_collapse(bool use_rest = false, bool to_complete = false, bool edge_collapse = false){

        this->edges.clear();
        std::vector< Vertex > save_vertices =
            std::vector<Vertex>(simplified_vertices);
        std::vector< Triangle > save_triangles =
            std::vector<Triangle>(simplified_triangles);
        EdgeQueue edgequeue;
        std::map<unsigned int, Eigen::MatrixXd> accumulateur;

        //EQEM
        for(unsigned int _i = 0; _i < simplified_vertices.size(); ++_i){
            accumulateur[_i]=Eigen::MatrixXd(4,4);
            accumulateur[_i].setZero();
        }
        for(unsigned int _i = 0; _i < simplified_vertices.size(); ++_i){
            accumulateur[_i] += Utilities().get_k(_i, simplified_vertices, simplified_triangles);
        }

        /*
            for(auto it = accumulateur.begin() ; it != accumulateur.end(); ++it){
                std::cout << it->first << std::endl << it->second << std::endl;
            }
        */

        std::cout << "Begin Init" << std::endl;
        set_edgequeue(edgequeue, simplified_vertices, simplified_triangles, accumulateur);

        std::cout << "There is edges : "
                  << edgequeue.edgePriorityQueue.size() << std::endl;
        std::cout << "Begin collapse" << std::endl;

        while(simplified_triangles.size() > 0){
            while(!edgequeue.isEmpty()){
                cost_edge ed = edgequeue.pop();
                if(edgequeue.isOk(ed)){
                    int to_collapse = 0;
                    int to_delete = 0;

                    for(Triangle _t : simplified_triangles){
                        if(_t[0] == ed.edge.i2 ||
                            _t[1] == ed.edge.i2 ||
                            _t[2] == ed.edge.i2){

                            if((_t[0] == ed.edge.i1 ||
                                 _t[1] == ed.edge.i1 ||
                                 _t[2] == ed.edge.i1)
                                &&
                                (_t[0] != _t[1] &&
                                 _t[1] != _t[2] &&
                                 _t[2] != _t[0])
                                ){++to_delete; }
                            else{++to_collapse;}
                        }
                    }

                    /*Collapse*/
                    for(unsigned int _t = 0 ; _t < simplified_triangles.size() ; ++_t){
                        /*Maj*/
                        if(simplified_triangles[_t][0] == ed.edge.i2 ||
                            simplified_triangles[_t][1] == ed.edge.i2 ||
                            simplified_triangles[_t][2] == ed.edge.i2){
                            /*
                                 *
                                 *     x  --- x             x-----x
                                 *   -   -  -       --->   -   -
                                 *  |x|__|x|             |x|-
                                 *
                                 *
                                 * */

                            if(simplified_triangles[_t][0] == ed.edge.i1 ||
                                simplified_triangles[_t][1] == ed.edge.i1 ||
                                simplified_triangles[_t][2] == ed.edge.i1){
                                simplified_triangles.erase(simplified_triangles.begin() + _t);
                                --_t;
                            }

                            /*
                                 *        x                        x
                                 *        |  -                  -     -
                                 *        |    -    ----->   -           -
                                 *  x     x ___ x          x---------------x
                                 *
                                 *
                                 * */

                            else{
                                /*where is Brian */
                                unsigned int targetIndex = 0;
                                for (int i = 0; i < 3; ++i) {
                                    if (simplified_triangles[_t][i] == ed.edge.i2) {
                                        targetIndex = i;
                                    }
                                }

                                /*Maj*/
                                simplified_triangles[_t][targetIndex] =
                                    ed.edge.i1;

                            }
                            accumulateur[ed.edge.i1] +=
                                accumulateur[ed.edge.i2];
                        }
                    }
                    /*Maj*/
                    edgequeue.disable(ed.edge.i2);
                    if(!use_rest){
                        if(!form_triangle(simplified_triangles, ed.edge.i1)){
                            this->edges.push_back(ed.edge);
                        }
                    }
                    else{
                        if(to_collapse <= 0 && to_delete > 0){
                            this->edges.push_back(ed.edge);
                        }
                    }

                    /* //Not good//
                    edgequeue.clean();
                    set_edgequeue(edgequeue, simplified_vertices, simplified_triangles, accumulateur);
                    */
                }
            }
            //std::cout << "Left triangles : "
            //<< simplified_triangles.size() << std::endl;
            set_edgequeue(edgequeue, simplified_vertices, simplified_triangles, accumulateur);
            //std::cout << "Adding new set of edges : "
            //<< edgequeue.edgePriorityQueue.size() << std::endl;
        }

        std::cout << "Begin Extraction" << std::endl;

        if(to_complete){

            Vertex barycenter(0.0, 0.0, 0.0);
            std::set<unsigned int > current_vertices;
            for(const Edge& e : this->edges){
                current_vertices.insert(e.i1);
                current_vertices.insert(e.i2);
            }
            for (const unsigned int& _i : current_vertices) {
                barycenter += simplified_vertices[_i];
            }
            barycenter /= current_vertices.size();

            std::cout << "Barycentre" << barycenter << std::endl;

            std::vector< Edge > copy(this->edges);

            std::set<unsigned int> _v;
            for(const Edge& ed : this->edges){
                _v.insert(ed.i1);
                _v.insert(ed.i2);
            }


            std::vector< Edge > _add;
            for(const Edge& ed : this->edges){
                int number = 0;
                for(const Edge &e : copy){
                    if((e.i1 == ed.i1 && e.i2 != ed.i2) &&
                            (e.i1 == ed.i2 && e.i2 != ed.i1)){
                        ++number;
                    }
                }
                if(number > 1){
                    ;
                }
                else{
                    double d1 = FLT_MAX;
                    int indice1 = -1;
                    double lim1 = std::sqrt(
                            std::pow(simplified_vertices[ed.i1][0] - barycenter[0], 2)
                            + std::pow(simplified_vertices[ed.i1][1] - barycenter[1], 2)
                            + std::pow(simplified_vertices[ed.i1][2] - barycenter[2], 2));
                    double d2 = FLT_MAX;
                    int indice2 = -1;
                    double lim2 = std::sqrt(
                            std::pow(simplified_vertices[ed.i2][0] - barycenter[0], 2)
                            + std::pow(simplified_vertices[ed.i2][1] - barycenter[1], 2)
                            + std::pow(simplified_vertices[ed.i2][2] - barycenter[2], 2));

                    std::set<unsigned int> already1;
                    already1.insert(ed.i1);
                    already1.insert(ed.i2);
                    std::set<unsigned int> already2;
                    already2.insert(ed.i1);
                    already2.insert(ed.i2);

                    std::set<unsigned int> next1;
                    next1.insert(ed.i1);
                    std::set<unsigned int> next2;
                    next2.insert(ed.i2);

                    while(next1.size() > 0){
                        auto it = next1.begin();
                        unsigned int firstElement = *it;
                        next1.erase(it);

                        for(const Edge& e : copy){
                            if(e.i1 == firstElement){
                                if(already1.find(e.i2) == already1.end()){
                                    already1.insert(e.i2);
                                    next1.insert(e.i2);
                                }
                            }
                            else if(e.i2 == firstElement){
                                if(already1.find(e.i1) == already1.end()){
                                    already1.insert(e.i1);
                                    next1.insert(e.i1);
                                }
                            }
                        }
                    }

                    while(next2.size() > 0){
                        auto it = next2.begin();
                        unsigned int firstElement = *it;
                        next2.erase(it);

                        for(const Edge& e : copy){
                            if(e.i1 == firstElement){
                                if(already2.find(e.i2) == already2.end()){
                                    already2.insert(e.i2);
                                    next2.insert(e.i2);
                                }
                            }
                            else if(e.i2 == firstElement){
                                if(already2.find(e.i1) == already2.end()){
                                    already2.insert(e.i1);
                                    next1.insert(e.i1);
                                }
                            }
                        }
                    }

                    for(unsigned int _i : _v){
                        if(_i != ed.i1 && _i != ed.i2 &&
                           (already1.find(_i) == already1.end())){
                            float dist = std::sqrt(
                              std::pow(simplified_vertices[ed.i1][0] - simplified_vertices[_i][0],2)
                              + std::pow(simplified_vertices[ed.i1][1] - simplified_vertices[_i][1],2)
                              + std::pow(simplified_vertices[ed.i1][2] - simplified_vertices[_i][2],2));
                            if(dist < d1 && dist <= lim1){
                                d1 = dist;
                                indice1 = _i;
                            }
                        }
                    }
                    for(unsigned int _i : _v){
                        if(_i != ed.i2 && _i != ed.i1 &&
                           (already2.find(_i) == already2.end())){
                            float dist = std::sqrt(
                              std::pow(simplified_vertices[ed.i2][0] - simplified_vertices[_i][0],2)
                              + std::pow(simplified_vertices[ed.i2][1] - simplified_vertices[_i][1],2)
                              + std::pow(simplified_vertices[ed.i2][2] - simplified_vertices[_i][2],2));
                            if(dist < d2 && dist <= lim2){
                                d2 = dist;
                                indice2 = _i;
                            }
                        }
                    }
                    if(indice1 != -1){
                        _add.push_back(Edge(ed.i1, indice1));
                        copy.push_back(Edge(ed.i1, indice1));
                    }
                    if(indice2 != -1){
                        _add.push_back(Edge(ed.i2, indice2));
                        copy.push_back(Edge(ed.i2, indice2));
                    }
                }
            }

            for(Edge& ed : _add){
                this->edges.push_back(ed);
            }
        }

        std::cout << "Getting coordinate" << std::endl;

        for(Edge& ed : this->edges){
            ed.v1 = simplified_vertices[ed.i1];
            ed.v2 = simplified_vertices[ed.i2];
            std::cout << "<" << ed.i1 << ","
                      << ed.i2 << ">"
                      << "   <--->   ("
                      << ed.v1.p << "  |  "
                      << ed.v2.p << ")"
                      << std::endl;
        }

        std::cout << "Begin Cleanup" << std::endl;

        std::vector< Edge > clean_edges;
        for(const Edge& ed : this->edges){
            int cpt = 0;
            for(const Edge& e : this->edges){
                if(ed.sym_equal(e)){
                    ++cpt;
                }
                if(cpt > 1) break;
            }
            if( cpt == 1 ){
                clean_edges.push_back(ed);
            }
        }
        this->edges = clean_edges;

        std::cout << "Begin second collapse" << std::endl;
        if(edge_collapse){
            bool end = true;
            do{
                end = true;
                std::map<unsigned int, unsigned int> collapse;
                for(unsigned int _i = 0; _i < this->edges.size(); ++_i){
                    collapse[this->edges[_i].i1] = this->edges[_i].i1;
                    collapse[this->edges[_i].i2] = this->edges[_i].i2;
                }
                for(unsigned int _i = 0; _i < this->edges.size(); ++_i){
                    const Edge& _e=this->edges[_i];
                    std::vector<unsigned int> voisins_i;
                    std::vector<unsigned int> voisins_j;

                    for(const Edge& _ed : this->edges){
                        if(!_ed.sym_equal(_e)){
                            if(_ed.i1 == _e.i1){
                                voisins_i.push_back(_ed.i2);
                            }
                            else if(_ed.i2 == _e.i1){
                                voisins_i.push_back(_ed.i1);
                            }
                            if(_ed.i2 == _e.i2){
                                voisins_j.push_back(_ed.i1);
                            }
                            else if(_ed.i1 == _e.i2){
                                voisins_j.push_back(_ed.i2);
                            }
                        }
                    }

                    int to_collapse = -1;
                    for(unsigned int _i = 0 ; _i < voisins_i.size() ; ++_i){
                        for(unsigned int _j = 0 ; _j < voisins_j.size() ; ++_j){
                            if(voisins_i[_i] == voisins_j[_j]){
                                to_collapse = voisins_i[_i];
                                break;
                            }
                        }
                        if(to_collapse != -1) {
                            break;
                            end = false;
                        }
                    }

                    if(to_collapse != -1){
                        Vertex _v(0.0f, 0.0f, 0.0f);
                        _v += vertices[_e.i1];
                        _v += vertices[_e.i2];
                        _v += vertices[to_collapse];
                        _v /= 3.0f;
                        this->vertices.push_back(_v);

                        for(unsigned int _j = 0; _j < this->edges.size(); ++_j){
                            Edge& _ed = this->edges[_j];
                            if(_ed.i1 == _e.i1
                                    || _ed.i1 == _e.i2
                                    || _ed.i1 == ((unsigned int)to_collapse)){
                                collapse[_ed.i1] = vertices.size() - 1;
                            }
                            else{
                                collapse[_ed.i1] = _ed.i1;
                            }
                            if(_ed.i2 == _e.i1
                                    || _ed.i2 == _e.i2
                                    || _ed.i2 == ((unsigned int)to_collapse)){
                                collapse[_ed.i2] = vertices.size() - 1;
                            }
                            else{
                                collapse[_ed.i2] = _ed.i2;
                            }
                        }
                    }
                }

                for(unsigned int _i = 0; _i < this->edges.size(); ++_i){
                    Edge& _ed = this->edges[_i];
                    if(collapse[_ed.i1] == collapse[_ed.i2]) {
                        this->edges.erase(this->edges.begin() + _i);
                        --_i;
                    }
                    else if(collapse[_ed.i1] != _ed.i1){
                        _ed.i1 = collapse[_ed.i1];
                        _ed.v1 = vertices[collapse[_ed.i1]];
                    }
                    else if(collapse[_ed.i2] != _ed.i2){
                        _ed.i2 = collapse[_ed.i2];
                        _ed.v2 = vertices[collapse[_ed.i2]];
                    }
                }
            } while(!end);
        }

        std::set<unsigned int> verticesInEdges;
        for (const Edge& edge : edges) {
            verticesInEdges.insert(edge.i1);
            verticesInEdges.insert(edge.i2);
        }

        std::vector<Vertex> newVertices;
        for (const unsigned int _i : verticesInEdges) {
            newVertices.push_back(simplified_vertices[_i]);
        }
        simplified_vertices.clear();
        simplified_vertices = newVertices;

        /* Test display */
        simplified_vertices = save_vertices;
        simplified_triangles = save_triangles;

        return;
    }

    int indexOfArticulation(std::vector<Articulation> _arti, point3d v)
    {
        for(int i = 0; i < _arti.size(); i ++)
        {
            point3d a_p = _arti[i].p;
            if(a_p[0] == v[0] && a_p[1] == v[1] && a_p[2] == v[2])
            {
                return i;
            }
        }

        return -1;
    }

    void load_skelete()
    {
        skeleton = Skelete();
        //skeleton.load("./Draco.skel");
        //std::vector<point3d> a;
        //a.push_back(point3d(0,0,0));
        //a.push_back(point3d(0,0,1));
        //a.push_back(point3d(0,1,0));
        //a.push_back(point3d(1,0,0));
        //skeleton.create(a);

        //articulation and bone from edge
        std::vector<Articulation> _articulations;
        std::vector<Bone> _bones;
        unsigned int e_size = edges.size();
        for(int i = 0; i < e_size; i ++)
        {
            int joint1 = 0;
            int joint2 = 0;
            //joint 1
            joint1 = indexOfArticulation(_articulations, edges[i].v1.p);
            if(joint1 == -1) {
                Articulation a = Articulation(edges[i].v1.p);
                _articulations.push_back(a);
                joint1 = _articulations.size() - 1;
            }

            //joint 2
            joint2 = indexOfArticulation(_articulations, edges[i].v2.p);
            if(joint2 == -1) {
                Articulation a = Articulation(edges[i].v2.p);
                _articulations.push_back(a);
                joint2 = _articulations.size() - 1;
            }

            //bone
            Bone b = Bone(joint1, joint2);
            _bones.push_back(b);
        }

        skeleton.articulations = _articulations;
        skeleton.bones = _bones;
        skeleton.buildStructure();
    }

    int pick_articulation(point3d p)
    {
        for (unsigned int i = 0; i < skeleton.articulations.size (); i++) {
            Articulation v = skeleton.articulations[i];
            //fixed distance
            if((v.p - p).norm() < 0.15)
            {
                return i;
            }
        }

        return -1;
    }

    int add_articulation(point3d p, int selected)
    {
        int articulation_size = skeleton.articulations.size();
        skeleton.articulations.push_back(Articulation(p));
        for(int i = 0; i < articulation_size; i ++)
        {
            skeleton.articulations[i].fatherBone = -1;
            skeleton.articulations[i].childBones.clear();
        }

        //update bones
        unsigned int bones_size = skeleton.bones.size();
        Bone new_bone = Bone(selected, articulation_size);
        skeleton.bones.push_back(new_bone);
        for(unsigned int i = 0; i < bones_size; i ++)
        {
            skeleton.bones[i].fatherBone = -1;
            skeleton.bones[i].childBones.clear();
        }

        //skeleton.buildStructure();

        return  articulation_size;
    }

    void update_articulation(int i, point3d p)
    {
        point3d diff = p - skeleton.articulations[i].p;
        skeleton.articulations[i].p = p;
        if(skeleton.articulations[i].v_indices.size()>0){
           Utilities().translateArticulation(skeleton.articulations[i], diff, vertices);
        }
    }

    void delete_articulation(int id)
    {
        unsigned int bones_size = skeleton.bones.size();

        if(bones_size <= 0
                || skeleton.articulations.size() <= 0) return;

        //delete artticulation
        skeleton.articulations.erase(skeleton.articulations.begin()+id);

        for(int i = 0; i < skeleton.articulations.size(); i ++)
        {
            skeleton.articulations[i].fatherBone = -1;
            skeleton.articulations[i].childBones.clear();
        }

        std::vector< Bone > new_bones;
        //update bones
        for(unsigned int i = 0; i < bones_size; i ++)
        {
            Bone cur_bone = skeleton.bones[i];
            if(cur_bone.joints[0] == id )
            {
                //update new father
                int a_father = skeleton.bones[cur_bone.fatherBone].joints[0];
                int joint2 = cur_bone.joints[1];
                if(a_father > id) a_father--;
                if(joint2 > id) joint2--;

                Bone n_bone = Bone(a_father, joint2);
                new_bones.push_back(n_bone);

            } else if(cur_bone.joints[1] != id)
            {
                int joint1 = cur_bone.joints[0];
                int joint2 = cur_bone.joints[1];
                if(joint1 > id) joint1--;
                if(joint2 > id) joint2--;

                Bone n_bone = Bone(joint1, joint2);
                new_bones.push_back(n_bone);
            }
        }
        skeleton.bones = new_bones;

        skeleton.buildStructure();
    }

    void draw_skelete(int selected_articulation)
    {
        skeleton.draw(selected_articulation);
    }

    bool contain(std::vector<unsigned int> const & i_vector, unsigned int element) {
        for (unsigned int i = 0; i < i_vector.size(); i++) {
            if (i_vector[i] == element) return true;
        }
        return false;
    }

    void collect_one_ring (std::vector<Vertex> const & i_vertices,
                           std::vector< Triangle > const & i_triangles,
                           std::vector<std::vector<unsigned int> > & o_one_ring) {
        o_one_ring.clear();
        o_one_ring.resize(i_vertices.size()); //one-ring of each vertex, i.e. a list of vertices with which it shares an edge
        //Parcourir les triangles et ajouter les voisins dans le 1-voisinage
        //Attention verifier que l'indice n'est pas deja present
        for (unsigned int i = 0; i < i_triangles.size(); i++) {
            //Tous les points opposés dans le triangle sont reliés
            for (int j = 0; j < 3; j++) {
                for (int k = 0; k < 3; k++) {
                    if (j != k) {
                        if (!contain(o_one_ring[i_triangles[i][j]], i_triangles[i][k])) {
                            o_one_ring[i_triangles[i][j]].push_back(i_triangles[i][k]);
                        }
                    }
                }
            }
        }
    }


    //cotangent

    Eigen::MatrixXd calc_L() {
        unsigned int v_size = simplified_vertices.size();
        unsigned int t_size = simplified_triangles.size();

        Eigen::MatrixXd L(v_size, v_size);
        L = 0 * L;
        for(unsigned int i = 0; i < t_size; i ++) {
            int v0 = simplified_triangles[i][0];
            int v1 = simplified_triangles[i][1];
            int v2 = simplified_triangles[i][2];

            //vector
            point3d v0v1 = simplified_vertices[v0].p - simplified_vertices[v1].p;
            point3d v0v2 = simplified_vertices[v0].p - simplified_vertices[v2].p;

            point3d v1v0 = simplified_vertices[v1].p - simplified_vertices[v0].p;
            point3d v1v2 = simplified_vertices[v1].p - simplified_vertices[v2].p;

            point3d v2v0 = simplified_vertices[v2].p - simplified_vertices[v0].p;
            point3d v2v1 = simplified_vertices[v2].p - simplified_vertices[v1].p;

            //cotan
            float cot1 = point3d::dot(v1v0, v2v0) / std::max((point3d::cross(v1v0, v2v0)).norm(), 1e-06);
            float cot2 = point3d::dot(v2v1, v0v1) / std::max((point3d::cross(v2v1, v0v1)).norm(), 1e-06);
            float cot3 = point3d::dot(v0v2, v1v2) / std::max((point3d::cross(v0v2, v1v2)).norm(), 1e-06);

            L(v1, v1) -= cot1;
            L(v2, v2) -= cot1;
            L(v1, v2) += cot1;
            L(v2, v1) += cot1;

            L(v0, v0) -= cot2;
            L(v2, v2) -= cot2;
            L(v2, v0) += cot2;
            L(v0, v2) += cot2;

            L(v0, v0) -= cot3;
            L(v1, v1) -= cot3;
            L(v0, v1) += cot3;
            L(v1, v0) += cot3;


        }

        return L;
    }

    float calc_triangle_area(int v0, int v1, int v2)
    {
        point3d a = simplified_vertices[v1].p - simplified_vertices[v0].p;
        point3d b = simplified_vertices[v2].p - simplified_vertices[v0].p;

        float area = (point3d::cross(a, b)).norm() / 2.0f;

        return area;
    }

    Eigen::MatrixXd calc_ring_areas() {

        unsigned int v_size = simplified_vertices.size();
        Eigen::MatrixXd original_areas(v_size, v_size);
        original_areas = 0 * original_areas;
        std::vector<std::vector<unsigned int>> voisins;
        collect_one_ring(simplified_vertices, simplified_triangles, voisins);

        for(unsigned int i = 0; i < v_size; i ++) {
            float area = 0;
            unsigned int voisin_size = voisins[i].size();

            for(unsigned int k = 0; k < voisin_size - 1; k++) {
                float cur_area = calc_triangle_area(i, voisins[i][k], voisins[i][k + 1]);
                 area += cur_area;
            }
            area += calc_triangle_area(i, voisins[i][0], voisins[i][voisin_size - 1]);
            original_areas(i, i) = area;
        }

        return original_areas;
    }

    float calc_avarage_area(){
        unsigned int t_size = simplified_triangles.size();
        float sum = 0;

        for(int i = 0; i < t_size; i ++)
        {

            Triangle c_triangle = simplified_triangles[i];
            float area = calc_triangle_area(c_triangle[0], c_triangle[1], c_triangle[2]);
            sum += area;
        }

        sum /= t_size;

        return sum;
    }

    float max_triangle_area() {
        unsigned int t_size = simplified_triangles.size();
        float max = 0;

        for(int i = 0; i < t_size; i ++)
        {
            Triangle c_triangle = simplified_triangles[i];
            float area = calc_triangle_area(c_triangle[0], c_triangle[1], c_triangle[2]);
            if(max < area) max = area;
        }

        return max;
    }

    Eigen::MatrixXd get_vertice_matrice() {
        unsigned int v_size = simplified_vertices.size();

        Eigen::MatrixXd v(v_size, 3);
        for(int i = 0; i < v_size; i ++) {
            v(i, 0) = simplified_vertices[i][0];
            v(i, 1) = simplified_vertices[i][1];
            v(i, 2) = simplified_vertices[i][2];
        }

        return v;
    }

    void UpdateVertices(Eigen::MatrixXd mV) {
            unsigned int v_size = simplified_vertices.size();
            for(int i = 0; i < v_size; i ++) {
               if(mV(i, 0) == NAN || mV(i, 1) == NAN || mV(i, 2) == NAN) {
                     continue;
               }
               simplified_vertices[i][0] = mV(i, 0);
               simplified_vertices[i][1] = mV(i, 1);
               simplified_vertices[i][2] = mV(i, 2);
            }
        }

    Eigen::MatrixXd getAreaRatios(Eigen::MatrixXd o_ring_area) {
        unsigned int v_size = simplified_vertices.size();
        Eigen::MatrixXd c_ring_area = calc_ring_areas();
        Eigen::MatrixXd ratio(v_size, v_size);
        ratio = 0 * ratio;
        for(int i = 0; i < v_size; i ++) {
            if(c_ring_area(i, i) > 0.00001) {
                ratio(i, i) = sqrt(o_ring_area(i , i) / c_ring_area(i , i));
            } else {
                ratio(i, i) = sqrt(1000000);
            }
        }

        return ratio;
    }


    void add_sleketon(int _iters = 20) {
        if(simplified_vertices.size() == 0){
            std::cout << "need simplify mesh first" << std::endl;
            return;
        }

        float WH = 1.0f;
        float WL = 0.01 * sqrt(calc_avarage_area()); //torus
        float sL = 2.0f;
        int n = simplified_vertices.size();
        // Initialize a dynamic-size vector
        Eigen::VectorXd vWH = Eigen::VectorXd::Constant(n, WH);
        Eigen::VectorXd vWL = Eigen::VectorXd::Constant(n, WL);

        // Create a diagonal matrix from the vector
        Eigen::MatrixXd mWH = vWH.asDiagonal();
        Eigen::MatrixXd mWL = vWL.asDiagonal();

        //ring area
        Eigen::MatrixXd o_ring_area = calc_ring_areas();
        Eigen::MatrixXd ratio = getAreaRatios(o_ring_area);

        //_iters = 20; //bitorus
        //_iters = 10; //beau gross
        //_iters = 20; //personnage
        //_iters = 10; //torus
        //_iters = 10; //dog

        unsigned int v_size = simplified_vertices.size();
        std::cout << v_size << std::endl;

        for(unsigned int it = 0; it < _iters; it ++) {
            Eigen::MatrixXd L = calc_L();
            Eigen::MatrixXd A(n * 2, n);
            A.topLeftCorner(n,n) = mWL * L;
            A.bottomLeftCorner(n,n) = mWH * ratio;

            Eigen::MatrixXd cur_vertices = get_vertice_matrice();

            Eigen::MatrixXd B(n * 2, 3);
            B.topLeftCorner(n,3) = 0 * cur_vertices;
            B.bottomLeftCorner(n,3) = mWH * ratio * cur_vertices;
            Eigen::MatrixXd res = (A.transpose() * A).ldlt().solve(A.transpose() * B);
            UpdateVertices(res);

            mWL = sL * mWL;
            ratio = getAreaRatios(o_ring_area);

            std::cout << it << std::endl;
            float max_area = max_triangle_area();
            if(max_area < 0.0001)
                break;
        }
    }


    void simplify(unsigned int resolution) {
        point3d aabb_max = point3d();
        point3d aabb_min = point3d();

        calculateBoundingBox(aabb_max, aabb_min);
        //
        std::vector<Voxel> voxels( resolution * resolution * resolution );
        calculateVoxels(voxels, resolution, aabb_max, aabb_min);

        //changement topology
        std::vector< Triangle > voxel_triangles;
        std::vector< int > voxel_vertices;
        for(unsigned int i = 0; i < triangles.size(); i ++) {
            int voxel_id_p1 = voxelIdOfVertex( vertices[triangles[i][0]], resolution, aabb_max, aabb_min);
            int voxel_id_p2 = voxelIdOfVertex( vertices[triangles[i][1]], resolution, aabb_max, aabb_min);
            int voxel_id_p3 = voxelIdOfVertex( vertices[triangles[i][2]], resolution, aabb_max, aabb_min);

            if(voxel_id_p1 != voxel_id_p2 && voxel_id_p2 != voxel_id_p3 && voxel_id_p1 != voxel_id_p3) {

                if(isExistInVector(voxel_vertices, voxel_id_p1) == false) {
                    voxel_vertices.push_back(voxel_id_p1);
                }

                if(isExistInVector(voxel_vertices, voxel_id_p2) == false) {
                    voxel_vertices.push_back(voxel_id_p2);
                }

                if(isExistInVector(voxel_vertices, voxel_id_p3) == false) {
                    voxel_vertices.push_back(voxel_id_p3);
                }

                int p1_index = indexOf(voxel_vertices, voxel_id_p1);
                int p2_index = indexOf(voxel_vertices, voxel_id_p2);
                int p3_index = indexOf(voxel_vertices, voxel_id_p3);

                voxel_triangles.push_back(Triangle( p1_index, p2_index, p3_index));
            }
        }

        //update vertices, normal and triangles
        std::vector< Vertex > new_vertices;
        unsigned int vertice_size = voxel_vertices.size();
        for(unsigned int i = 0; i < vertice_size; i ++) {
            point3d v = voxels[voxel_vertices[i]].vertex;
            new_vertices.push_back(Vertex(v[0], v[1], v[2]));
        }

        std::cout << "nb vertices:" << vertices.size() << " to " << new_vertices.size() << std::endl;
        std::cout << "nb triangles:" << triangles.size() << " to " << voxel_triangles.size() << std::endl;

        simplified_vertices = new_vertices;
        simplified_triangles = voxel_triangles;

    }

    bool isExistInVector(std::vector<int> vector, int value ) {
        unsigned int vector_size = vector.size();
        for(unsigned int i = 0; i < vector_size; i ++) {
            if(vector[i] == value) {
                return true;
            }
        }

        return false;
    }

    int indexOf(std::vector<int> vector, int value ) {
        unsigned int vector_size = vector.size();
        for(unsigned int i = 0; i < vector_size; i ++) {
            if(vector[i] == value) {
                return i;
            }
        }
        return -1;
    }

    int voxelIdOfVertex(point3d point, unsigned int resolution, point3d aabb_max, point3d aabb_min) {
        //calculate dimension of AABB
        float dimension_aabb_x = aabb_max[0] - aabb_min[0];
        float dimension_aabb_y = aabb_max[1] - aabb_min[1];
        float dimension_aabb_z = aabb_max[2] - aabb_min[2];
        //
        int segment_x = ((point[0] - aabb_min[0]) / dimension_aabb_x) * (resolution - 1);
        int segment_y = ((point[1] - aabb_min[1]) / dimension_aabb_y) * (resolution - 1);
        int segment_z = ((point[2] - aabb_min[2]) / dimension_aabb_z) * (resolution - 1);

        //
        return segment_x + segment_y * resolution + segment_z * (resolution * resolution);
    }

    void calculateVoxels(std::vector<Voxel> &voxels, unsigned int resolution, point3d aabb_max, point3d aabb_min) {

        for(unsigned int i = 0; i < vertices.size(); i ++) {
            //
            int voxel_id = voxelIdOfVertex(vertices[i], resolution, aabb_max, aabb_min);
            voxels[voxel_id].vertex += vertices[i];
            voxels[voxel_id].vertex_count += 1;
        }

        unsigned int voxel_size = voxels.size();
        for(unsigned int k = 0; k < voxel_size; k ++) {
            if(voxels[k].vertex_count > 0) {
                voxels[k].vertex /= voxels[k].vertex_count;
            }
        }

    }

    void calculateBoundingBox(point3d &aabb_max, point3d &aabb_min) {
        //calculer AABB
        aabb_max = vertices[0];
        aabb_min = vertices[0];

        for(unsigned int i = 1; i < vertices.size(); i ++) {
            //calc max
            if(aabb_max[0] < vertices[i][0] ) {
                aabb_max[0] = vertices[i][0];
            }

            if(aabb_max[1] < vertices[i][1] ) {
                aabb_max[1] = vertices[i][1];
            }

            if(aabb_max[2] < vertices[i][2] ) {
                aabb_max[2] = vertices[i][2];
            }

            //calc min
            if(aabb_min[0] > vertices[i][0] ) {
                aabb_min[0] = vertices[i][0];
            }

            if(aabb_min[1] > vertices[i][1] ) {
                aabb_min[1] = vertices[i][1];
            }

            if(aabb_min[2] > vertices[i][2] ) {
                aabb_min[2] = vertices[i][2];
            }
        }
    }
    void connect_keypoints(unsigned int joint1, unsigned int joint2)
    {
        unsigned int bones_size = skeleton.bones.size();
        for(unsigned int i = 0; i < bones_size; i ++)
        {
            Bone b = skeleton.bones[i];
            if(b.joints[0] == joint1 && b.joints[1] == joint2) return;
            if(b.joints[0] == joint2 && b.joints[1] == joint1) return;
        }

        Bone new_bone = Bone(joint1, joint2);
        skeleton.bones.push_back(new_bone);
    }

    bool form_triangle(std::vector<Triangle> const & T,
                       unsigned int i){
        for(Triangle _t : T){
            if(_t[0] == i || _t[1] == i || _t[2] == i){
                return true;
            }
        }
        return false;
    }

    bool form_triangleABC(std::vector<Triangle> const & T,
                          Edge _e){
        for(Triangle _t : T){
            if(
                (_t[0] == _e.i1 && (_t[1] == _e.i2 || _t[2] == _e.i2))
                ||
                (_t[1] == _e.i1 && (_t[0] == _e.i2 || _t[2] == _e.i2))
                ||
                (_t[2] == _e.i1 && (_t[0] == _e.i2 || _t[1] == _e.i2))
                ){return true;
            }
        }
        return false;
    }

    bool will_be_deleted(std::vector<Triangle> const & T,
                         unsigned int _t1, unsigned int _t2){
        int find = 0;
        for(Triangle _t : T){
            int t1_index = (_t[0] == _t1 ? 0 :
                                (_t[1] == _t1 ? 1 :
                                     (_t[2] == _t1 ? 2 : -1)));
            int t2_index = (_t[0] == _t2 ? 0 :
                                (_t[1] == _t2 ? 1 :
                                     (_t[2] == _t2 ? 2 : -1)));

            //Il existe au moins 1 triangle avec un seul sommet
            //-> collapse not erase
            if ((t1_index != -1 && t2_index == -1) ||
                (t2_index != -1 && t1_index == -1)) return false;

            //Forcément un triangle avec les deux sommets sinon return
            find += ((t1_index != -1 || t2_index == -1) ? 1 : 0);
        }

        //Ne vas pas l'être car il l'est déjà
        if(find <= 0) return false;

        //J'ai trouve au moins un triangle qui va être collapse ou erase
        //et il n'y aura collapse si boucle ne s'est pas terminée
        return true;
    }
};



#endif // PROJECTMESH_H
