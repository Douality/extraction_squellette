# Extraction de squelette par contraction de maillage

Ce projet de recherche se concentre sur l'implémentation d'une méthode d'extraction de squelette basée sur la contraction de maillage. Veuillez noter que l'application peut planter et que les instructions doivent être suivies attentivement.

## Instructions

1. Suivez les boutons en forme de squelette dans l'ordre de gauche à droite pour naviguer dans le pipeline.
Les squelettes de gauches sont responsables des phases de contractione et ceux excentrés à droite de l'extraction.
2. Les deux boutons en forme de squelette au plus à droite représentent 2 méthodes différentes avec des options. Celui de droite suit la méthode de l'article, et l'alternative est à gauche.
3. A la suite de l'article:
Le boutton le plus à droite possède une option supplémentaire dans l'optique de corriger un point notable de la méthode de l'article, méthode de correction elle-même citée dans l'article en question.
Les deux bouttons possèdent une option de 'reconnexion des edges' basées sur la stratégie de la reconstructon de la connectivité.
Cette option possède un bug connu, il crée un vertex connecté avec beaucoup d'edge alors qu'il devrait disparaitre automatiquement, à défaut le sélectionner avec la souris - clic -puis 'SUPPR'
4. Un Boutton en logo camera se trouve à droite après les squelletes, il permet de sauvegarder au format .skel (ARTICULATION SIZE() vertices - BONES SIZE() edges) 
5. Un Autre Boutton en logo + se trouve le plus à droite , il permet de charger un squellette au format .skel (ARTICULATION SIZE() vertices - BONES SIZE() edges) 

## Précautions

- Le chargement peut ne pas toujours réussir.
Ce problème apparait principalement après la simplification. Elle fonctionnera puis il est possible qu'il n'y ai aucun résultat dans la contraction. Dans ce cas (si `n` n'est pas trop grand), et aucun résultat n'est visible après contraction, redémarrez l'application, malheuressement autant de fois qu'il y aura ce problème( selon tout vraissemblance il vient du tampon car parfois c'est le mesh qui ne s'ouvre pas malgré la conversation du code de base fournis poir  l'ouverture) si on est 'excité de la souris') il peut - jamais - tout comme intervenir 5 fois d'affilée (même si nous n'avons pas vu plus de 2-3fois maximun dans le pire des cas).

## Raccourcis clavier

- Appuyez sur `H` pour obtenir de l'aide.
- Appuyez sur `T` pour changer le titre de la fenêtre (Ctrl + T).
- Appuyez sur `Delete` pour supprimer l'articulation sélectionnée.
- Appuyez sur `A` pour ajouter un point clé (lorsqu'une articulation est sélectionnée).
- Appuyez sur `E` pour connecter des points clés (lorsqu'une articulation est sélectionnée).
- Appuyez sur `M` pour basculer entre le maillage original et le maillage simplifié.
- Appuyez sur `W` pour basculer en mode fil de fer.
- Appuyez sur `D` pour basculer l'éclairage.

## PIPELINE

1. Boutton 'open mesh'
2. 1er boutton squelette à gauche 'Simplification'
3. 2er boutton squelette sur la droite du premier 'contraction'
   Spécifier n : Expérimentation n = 20 ( 80% ) ou n = 10 (ou n = 6 article)
   Il doit y avoir des îlot et des traits qui les relient
4. Plus a droite, les bouttons squelettes vers le flanc droit
   Celui de gauche utilise une méthode expérimentation bassé sur un article sur cette article
   Celui de droite cherche à rester sur la méthode stricte de l'article
   Deux bouttons : Option pour reconnecter automatiquement les edges ( coupés selon une méthode de construction spécifiques dans un autre article ).
   Boutton de droite : Option supplémentaire pour faire un post-edge ecollapse pour retirer certaines arrêtes intruses.
   Bug connu : Il peut créer un super point connecté à toutes les edges devant être supprimés, 
   Sélectionner ce point avec la souris 'SUPPR'
5. Deux bouttons sur la droite permettent de sauvegarder ua format .skel
(ARTICULATION SIZE() vertices - BONES SIZE() edges)  ou ouvrir un squelette pour reprend ou bien conserver un travail antérieur.
   

## Note importante

Ce projet est axé sur la recherche, et des problèmes inattendus peuvent survenir. Veuillez faire preuve de prudence, et en cas de plantage, redémarrez l'application.
